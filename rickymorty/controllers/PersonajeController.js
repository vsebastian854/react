const Personaje = require('../models/Personaje.js')

exports.verPersonajes = (req, res) => {
Personaje.find().then(personajes => {
    res.json({personajes});
})
};

exports.favorito = (req, res) => {
    let id = req.params.id;
    Personaje.find({name:"Adjudicator Rick"})
    .then(favorito => {
        res.json({favorito})
    })
}

exports.borrarPersonaje = (req, res) => {
let id = req.params.id;
console.log(id)
Personaje.findByIdAndDelete( req.params.id)
.then(personajeBorrado=>{}).catch(error => {res.send("No se ha podido borrar al personaje")})
}

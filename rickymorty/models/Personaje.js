const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const PersonajeSchema = new Schema({
    id: String, 
    name: String,
    status: String,
    species: String,
    type: String,
    gender: String,
    image: String,
    url: String,
    created: String
})

module.exports=mongoose.model('personajes', PersonajeSchema)

var express = require('express');
var router = express.Router();
var PersonajeController = require('../controllers/PersonajeController')

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('Bienvenido a la sección de Personajes :)');
});

/* GET users listing. */
router.get('/verPersonajes', PersonajeController.verPersonajes );
router.get("/borrarPersonaje/:id", PersonajeController.borrarPersonaje);
router.get("/favorito", PersonajeController.favorito);

module.exports = router;

import logo from './logo.svg';
import './App.css';
import { Inicio } from './componentes/inicio';
import { Deportes } from './componentes/Deportes';
import BarraNav from './componentes/BarraNavegacion';
import {BrowserRouter, Routes, Route} from 'react-router-dom'

function App() {
  return (
    <>
      <BrowserRouter>
      <Routes>
        <Route path="/" element ={<Inicio/>}></Route>
        <Route path="/deportes" element = {<Deportes/>}></Route>
        </Routes>
      </BrowserRouter>
      </>
  );
}

export default App;

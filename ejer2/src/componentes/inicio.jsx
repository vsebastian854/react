import BarraNav from "./BarraNavegacion";
import imagen from '../img/axel.jpg'
import "./inicio.css"
import { Contador } from "./ContadorUseState";



export function Inicio(){
    return (
        <>
        <BarraNav/>
            <div>
                <h1>INICIO</h1>
                <img src = {imagen} alt="Tormenta de Fuego" ></img>
                <Contador></Contador>
            </div>
        </>
    )
}
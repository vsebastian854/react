import React, {useState} from 'react';

export const Contador = ()=> {
    const [count, setCount] = useState(0);
  return (
    <div>
        <p>Has usado la Tormenta de Fuego por {count} º/ª vez*</p>
        <button onClick = {()=>setCount(count+1)}>Usar</button>
    </div>
  ) 
}
import { Button } from "react-bootstrap";
import 'bootstrap/dist/css/bootstrap.min.css';
import "./BarraNavegacion.css";
import { Inicio } from "./inicio";
import imagen from '../img/axel.jpg'
import "./BarraNavegacion.css"

export default function BarraNav (){
    let buttons = (
        <div>
            <Button href = "/" className="ms-4 mt-3 mb-2" variant="warning" >Inicio</Button>
            <Button href = "/deportes" className="ms-4 mt-3 mb-2" variant="warning" >Deportes</Button>
        </div>
    )
    return (
        <header>
            <nav>
                <ul>{buttons}</ul>
            </nav>
        </header>
    )
}